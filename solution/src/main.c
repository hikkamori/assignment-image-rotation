
#include "../include/rotation.h"
#include "../include/serial.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {

    if(argc != 3){
        fprintf(stderr, "not enough/extra arguments");
        return 1;
    }

	FILE *fp = fopen(argv[1], "rb");
    struct image img = {0};
    from_bmp(fp, &img);
    struct image newim = rotate(img);

    if(!newim.data){
        fprintf(stderr, "malloc hasn't worked properly");
        return 1;}

    FILE* fo = fopen(argv[2], "wb");
    to_bmp(fo, &newim);
    free(img.data);
    free(newim.data);
    fclose(fp);
    fclose(fo);
	return 0;
}

