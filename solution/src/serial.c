
#include "../include/image.h"
#include "../include/bmp_header.h"
#include "../include/serial.h"
#include <stdio.h>


const int defBfType = 19778; //Значение для файлов типа BMP
const int defBiSize = 40; //Размер заголовка
const int defBiPlanes = 1; //Одна плоскость 
const int defBiBitCount = 24; //Используем 24-х битное кодирование






enum write_status to_bmp(FILE* out, struct image const* img){

    struct bmp_header header = {
            .bfType = defBfType,
            .bfileSize = (sizeof(struct pixel) * ((img->width) + padding(img->width)))*img->height,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = defBiSize,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = defBiPlanes,
            .biBitCount = defBiBitCount
    };


    if (!fwrite(&header, 1, sizeof(struct bmp_header), out)){
        return WRITE_ERROR;
    }

    char garbage[4] = {0};

    for (size_t i = 0; i < img->height; i++)
    {
        if (fwrite(img->data + i*(img->width), sizeof(struct pixel), img->width, out) != img->width)
            return WRITE_ERROR;
        if (fwrite(garbage, sizeof(char), padding(img->width), out) != padding(img->width))
            return WRITE_ERROR;
    }
    return WRITE_OK;

}



//
// Created by Egor Sorokin on 30.10.2023.
//
