#include "../include/image.h"
#include "../include/rotation.h"
#include "../include/serial.h"
#include <stdlib.h>



struct image rotate(struct image source){
    struct image newimage = {source.height, source.width, (struct pixel*) malloc(source.width * source.height * sizeof(struct pixel))};

    if(source.data) {
        for (size_t i = 0; i < source.height; i++) {
            for (size_t j = 0; j < source.width; j++) {
                newimage.data[source.height * (j + 1) - (i + 1)] =
                        source.data[source.width * i + j];
            }
        }


    }
    return newimage;
}
//
// Created by Egor Sorokin on 30.10.2023.
