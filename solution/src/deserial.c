
#include "../include/image.h"
#include "../include/bmp_header.h"
#include "../include/serial.h"
#include <stdio.h>
#include <stdlib.h>




uint16_t htype = 19778;

enum read_status from_bmp(FILE* in, struct  image* img){
    struct bmp_header header = {0};
    if(fread(&header, sizeof(struct bmp_header), 1, in) != 1) {
        return READ_INVALID_HEADER;
    }

    img->width = header.biWidth;

    img->height = header.biHeight;


    img->data = (struct pixel*) malloc(img->width * img-> height * sizeof(struct pixel));

    if(!img -> data){
        return READ_MEMORY_ERROR;
    }

    for (size_t i = 0; i < img->height; i++) {
        if (fread(img->data + i * (img->width), sizeof(struct pixel), img->width, in) != img->width)
            return READ_INVALID_BITS;
        if(fseek(in, padding(img->width), SEEK_CUR))
            return READ_INVALID_BITS;
    }



    if(header.biBitCount != 24){
        free(img->data);
        return READ_INVALID_BITS;
    }


    if (header.bfType != htype)
    {
        free(img->data);
        return READ_INVALID_SIGNATURE;
    }

    return READ_OK;

}



//
// Created by Egor Sorokin on 30.10.2023.
//
