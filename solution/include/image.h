//
// Created by Egor Sorokin on 30.10.2023.
//


#ifndef PLAB_IMAGE_H
#define PLAB_IMAGE_H


#include <stdint.h>

struct image {
    uint32_t width, height;
    struct pixel* data;
};

struct pixel {uint8_t b, g, r; };


#endif //PLAB_IMAGE_H
