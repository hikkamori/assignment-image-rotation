//
// Created by Egor Sorokin on 30.10.2023.
//
#include "image.h"



#ifndef PLAB_ROTATION_H
#define PLAB_ROTATION_H

struct image rotate(struct image source);

#endif //PLAB_ROTATION_H
